const Sequelize = require('sequelize');
const serverConnection=require("/home/nagamani/moviesAndDirector/src/utils/connection.js").serverConnection;
let sequelize=serverConnection();
let moviesTable=sequelize.define("moviesTables",{
    Rank:Sequelize.INTEGER,

    Title:Sequelize.STRING,
    Description:Sequelize.STRING,
    Runtime:Sequelize.INTEGER,
    Genre:Sequelize.STRING,
    Rating:Sequelize.INTEGER,
    Metascore:Sequelize.INTEGER,
    Votes:Sequelize.INTEGER,
    Gross_Earning_in_Mil:Sequelize.INTEGER,
    Director:Sequelize.INTEGER,
    Actor:Sequelize.STRING,
    Year:Sequelize.INTEGER


},{})
moviesTable.findAll({
    raw:true,
    attributes:["Rank","Title","Description","Runtime","Genre","Rating","Metascore","Votes","Gross_Earning_in_Mil","Director","Actor","Year"]
}).then((result)=>{
    console.log(result);
    })