const Sequelize = require('sequelize');
const serverConnection = require("/home/nagamani/moviesAndDirector/src/utils/connection.js").serverConnection;
let sequelize = serverConnection();
const connection = require('/home/nagamani/moviesAndDirector/src/scripts/seedData.js').connection;
let app = require("/home/nagamani/moviesAndDirector/src/index.js").app;
let directorTable = sequelize.define("directorTables", {
    Director_id: {
        type: Sequelize.INTEGER,
        primaryKey: true
    },
    Director: Sequelize.STRING
}, {});
function togetTheDirectorData() {
    app.get('/api/directors', (request, response) => {
        directorTable.findAll({
            attributes: ['Director', 'Director_id']
        }).then((result) => {
            response.send(result);
        });
    });
}
togetTheDirectorData();
function getTheDirectorsWithId() {
    app.get('/api/directors/:directorId', (request, response) => {
        directorTable.findAll({
            attributes: ['Director', 'Director_id'],
            where: {
                Director_id: request.params.directorId
            }
        }).then((result) => response.send(result));
    })
}
getTheDirectorsWithId();
function addTheNewDirector() {
    app.post('/api/directors', (req, res) => {
        directorTable.create({
            Director: req.body.Director,
            createdAt: "0",
            updatedAt: "0"
        }).then((result) => res.send(result));
    })

}
addTheNewDirector();
