const express = require("express");
const directorsRouter = require("./router/directors");
const moviesRouter = require("./router/movies");
const bodyParser = require('body-parser')
const morgan = require('morgan');
var path = require('path');
var fs = require("fs");
var cors=require('cors')
var app = express();
app.use(express.static(path.join(__dirname, './build')));
app.get('/', function(req, res) {
  res.sendFile(path.join(__dirname, './build', 'index.html'));
});
app.use(cors());
app.use(bodyParser.json())
var accessLogStream = fs.createWriteStream(__dirname + '/access.log', {flags: 'a'});
app.use(morgan('combined', {stream: accessLogStream}));
app.use('/api', directorsRouter);
app.use('/api', moviesRouter);

app.listen(4000, () => console.log("working"));
