const connection = require('../scripts/seedData.js').connection;
const express = require('express');
const router = express.Router();
let winston = require("../utils/logging.js");
const validationForId = require('../middlewares/joiValidationScheme.js').validationForId;
const validationForPut = require('../middlewares/joiValidationScheme.js').validationForPutForMovies;
const validationForPost = require('../middlewares/joiValidationScheme.js').validationForPostForMovies;
router.get('/movies', (request, response, next) => {
    connection.query('SELECT * FROM moviesTables', (error, result) => {
        if (error) {
            winsto.error(error);
            throw error;
        }

        response.send(result);
    });
});
router.get('/movies/:movieId', (request, response, next) => {
    validationForId(request, response);
    connection.query('SELECT * FROM moviesTables WHERE Director= ?', [request.params.movieId], (error, rows, fileds) => {
        if (error) {
            winston.error(error);
            throw (error);
        }

        else {
            response.send(rows);

        }

    })
})
router.post('/movies', (req, res, next) => {
    validationForPost(req, res);
    let movies = req.body;
    let newMovieData = `INSERT INTO moviesTables(Rank,Title,Description,Runtime,Genre,Rating,Metascore,Votes,Gross_Earning_in_Mil,Director,Actor,Year) \
                       VALUES('${movies.Rank}','${movies.Title}','${movies.Description}','${movies.Runtime}','${movies.Genre}','${movies.Rating}','${movies.Metascore}','${movies.Votes}','${movies.Gross_Earning_in_Mil}','${movies.Director}','${movies.Actor}','${movies.Year}')`;
    connection.query(newMovieData, movies, (err, result, fileds) => {
        if (err) {
            winston.error(err);
            throw err;
        }
        res.send({
            status: "success",
            movies: movies
        })
    })
})
router.put('/movies/:movieId', (req, res, next) => {
    let movies = req.body;
    console.log(movies);
    validationForPut(req, res);
    let updateMovies = "UPDATE moviesTables SET Rank=?,Title=?,Description=?,Runtime=?,Genre=?,Rating=?,Metascore=?,Votes=?,Gross_Earning_in_Mil=?,Director=?,Actor=?,Year=? WHERE  Rank=?";
    connection.query(updateMovies,[req.body.Rank,req.body.Title,req.body.Description,req.body.Runtime,req.body.Genre,req.body.Rating,req.body.Metascore,req.body.Votes,req.body.Gross_Earning_in_Mil,req.body.Director,req.body.Actor,req.body.Year,req.params.movieId] ,(err, rows, fileds) => {
        if (err) {
            winston.error(err);
            throw err;
        }
        res.send({
            status: "success",
            movies: movies
        })
    })
})
router.delete('/movies/:movieId', (req, res, next) => {
    movies = req.params.movieId;
    validationForId(req, res);
    deleteMovie = "DELETE FROM moviesTables WHERE Rank=" + req.params.movieId + "";
    connection.query(deleteMovie, (err, result, fileds) => {
        if (err) {
            winston.error(err);
            throw err;
        }
        res.send({
            status: "deleted successfully",
            movies: movies
        })
    })
})
module.exports = router;