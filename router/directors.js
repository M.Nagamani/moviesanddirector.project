const connection = require('../scripts/seedData.js').connection;
const express = require('express');
let winston = require("../utils/logging.js");
const validationForId = require('../middlewares/joiValidationScheme.js').validationForId;
const validationForPutForDirectors = require('../middlewares/joiValidationScheme.js').validationForPutForDirectors;
const validationForPostForDirector = require('../middlewares/joiValidationScheme.js').validationForPostForDirector;
const router = express.Router();
router.get('/directors', (request, response, next) => {
    connection.query('SELECT * FROM directorTables', (error, result) => {
        if (error) {
            throw error;
        }

        response.send(result);
    });
});
router.get('/directors/:directorId', (request, response, next) => {
    validationForId(request, response);
    connection.query('SELECT * FROM directorTables WHERE Director_id= ?', [request.params.directorId], (error, result, fileds) => {
        if (error) {
            winston.error(error);
            throw error;
        }
        if (!error)
            response.send(result);

    })
})
router.post('/directors', (request, response, next) => {
    validationForPostForDirector(request, response);
    let directorBody = request.body;
    let newDirectorData = `INSERT INTO directorTables(Director) \
                              VALUES('${directorBody.Director}')`;
    connection.query(newDirectorData, directorBody, (err, result, fileds) => {
        if (err) {
            winston.error(err);
            throw err;
        }
        response.send({
            status: "success",
            directorBody: Object.assign(directorBody)

        })
    })
})

router.put('/directors/:director_id', (req, res, next) => {
    let directorReq = req.body;
    validationForPutForDirectors(req, res);
    let updateDirector = "UPDATE directorTables SET Director= '" + req.body.Director + "'WHERE Director_id=" + req.params.director_id;
    console.log(updateDirector);
    connection.query(updateDirector, (err, rows, fileds) => {
        res.send({
            status: "success",
            directorReq: Object.assign(directorReq)

        })
        if (err) {
            winston.error(error);
            throw err;
        }
    })
})
router.delete('/directors/:director_id', (req, res, next) => {
    deleteDir = req.params.director_id;
    validationForId(req, res);
    deleteDirector = "DELETE FROM directorTables WHERE Director_id= " + req.params.director_id + "";
    connection.query(deleteDirector, (err, result, fileds) => {
        if (err) {
            winston.error(error);
            throw err;
        }
        res.send({
            status: "deleted successfully",
            deleteDir: Object.assign(deleteDir)

        })
    })
})

module.exports = router;