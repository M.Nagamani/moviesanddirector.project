let connection = require("../utils/mysqlConnection.js").connection
var fs = require("fs");
var data = fs.readFileSync('../src/movies.json', 'utf8');
var obj = JSON.parse(data);
connection.connect(function (err) {
  if (err) console.log(err);
  dropTheTableForMovies();
  creatingTableForMovies();
  dropTheTableForDirectors();
  creatingTableForDirectors();
  creatingDirectorId();
});

// function creatingDatabase(){

//     let moviesDataBase="CREATE DATABASE movies";
//     connection.query(moviesDataBase,function(err,req){
//         if(err) 
//         throw err;
//         console.log("database created");
//     })
//     creatingTableForMovies();
//     }
function dropTheTableForMovies() {
  let moviesTable = "DROP TABLE moviesTables";
  connection.query(moviesTable, function (err, req) {
    if (err)
      throw err;
    //console.log("droping table is completed succesfully");
  })
}
function creatingTableForMovies() {
  let moviesDatabaseTable = "CREATE TABLE moviesTables(Rank INT NOT NULL,Title VARCHAR(100) NOT NULL,Description VARCHAR(500) NOT NULL,Runtime INT NOT NULL,Genre VARCHAR(50) NOT NULL,Rating VARCHAR(50) NOT NULL,Metascore VARCHAR(50) NOT NULL,Votes INT NOT NULL,Gross_Earning_in_Mil VARCHAR(50) NOT NULL,Director VARCHAR(50) NOT NULL,Actor VARCHAR(50) NOT NULL,Year INT NOT NULL)";
  connection.query(moviesDatabaseTable, function (err, req) {
    if (err)
      throw err;
  })
  insertingDataIntoTable();
}
function insertingDataIntoTable() {
  obj.forEach((element) => {
    let insertingIntoMoviesTable = `INSERT INTO moviesTables(Rank,Title,Description,Runtime,Genre,Rating,Metascore,Votes,Gross_Earning_in_Mil,Director,Actor,Year) VALUES("${element.Rank}","${element.Title}","${element.Description}","${element.Runtime}","${element.Genre}","${element.Rating}","${element.Metascore}","${element.Votes}","${element.Gross_Earning_in_Mil}","${element.Director}","${element.Actor}","${element.Year}")`;
    connection.query(insertingIntoMoviesTable, element, function (err, result) {
      if (err)
        throw err;
    })
  })
}
function dropTheTableForDirectors() {
  let DirectorTable = "DROP TABLE directorTables";
  connection.query(DirectorTable, function (err, req) {
    if (err)
      throw err;
    // console.log("droping table is completed succesfully");
  })
}
function creatingTableForDirectors() {
  let DirectorDatabaseTable = "CREATE TABLE directorTables(Director_id INT(11) AUTO_INCREMENT,Director VARCHAR(50) NOT NULL UNIQUE, PRIMARY KEY (`Director_id`))";
  connection.query(DirectorDatabaseTable, function (err, req) {
    if (err)
      throw err;
    // console.log("table is created");
  })
  insertingDataIntoDirectorTable();
}
function insertingDataIntoDirectorTable() {
  let newobj = [];
  obj.forEach((element) => {
    if (!newobj.includes(element.Director)) {
      let insertingIntoDirectorTable = `INSERT INTO directorTables(Director) VALUES("${element.Director}")`;
      connection.query(insertingIntoDirectorTable, element, function (err, result) {
        if (err)
          console.log(err);
      })
      newobj.push(element.Director);
    }
  })
}
function creatingDirectorId() {
  connection.query("UPDATE moviesTables INNER JOIN directorTables ON moviesTables.Director=directorTables.Director SET moviesTables.Director=directorTables.Director_Id", function (err, result, fields) {
    if (err) throw err;
    console.log(result);
  });
}
module.exports = {
  connection: connection
}