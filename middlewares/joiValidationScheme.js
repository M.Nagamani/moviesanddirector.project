let Joi = require("joi");
let winston = require("../utils/logging.js");
function validationForId(request, response) {
     moviesId = request.params.movieId;
    directorsId = request.params.directorId;
    let schema = {
         movieId: Joi.number().integer().min(1).max(55),
        directorId: Joi.number().integer().min(1).max(37)
    }
    Joi.validate(request.params.directorId, schema, (err, data, next) => {
        if (err) {
            winston.error(err);
            response.status(404).json({
                status: 'error',
                message: 'Bad Request',
                moviesId: moviesId,
                directorsId: directorsId
            });
            next();
        }
    })
}

function validationForPutForMovies(request, response) {
    let movies = request.body;
    let schema = {
        Rank:Joi.number().integer().positive().required(),
        Title: Joi.string().required(),
        Description: Joi.string().required(),
        Runtime: Joi.number().positive().required(),
        Genre: Joi.string().required(),
        Rating: Joi.number().positive().required(),
        Metascore: Joi.number().positive().required(),
        Votes: Joi.number().positive().required(),
        Gross_Earning_in_Mil: Joi.number().positive().required(),
        Director: Joi.number().positive().min(1).max(35).required(),
        Actor: Joi.string().required(),
        Year: Joi.number().positive().required()


    }
     Joi.validate(request.body, schema, (err, result, next) => {
        if (err) {
            winston.error(err);
            response.status(404).json({
                status: 'error',
                message: 'Not Found',
                movies: movies
            });
            next();
        }
    })
}
function validationForPutForDirectors(req, res) {
    let directorBody = req.body;
    const schema = {
        //  director_Id:Joi.number().integer(),
         Director: Joi.string().required()
    };
    Joi.validate(req.body, schema, (err, result, next) => {
        if (err) {
            console.log(err)
            winston.error(err);
            res.status(404).json({
                status: 'error',
                message: 'Not Found',
                directorBody: directorBody
            });
            next();
        }
    })

}
function validationForPostForDirector(req, res) {
    let directorBody = req.body;
    const schema = {
        directorId: Joi.number().integer().min(1).max(35),
        Director: Joi.string().alphanum().min(3).max(30).required()
    }
     Joi.validate(req.body, schema, (err, result, next) => {
        if (err) {
            winston.error(err);
            res.status(404).json({
                status: 'error',
                message: 'Not Found',
                directorBody: directorBody
            });
            next();
        }
    })

}
function validationForPostForMovies(request, response) {
    let movies = request.body;
    let schema = {
     Rank:Joi.number().integer().positive(),
        Title: Joi.string().required(),
        Description: Joi.string().required(),
        Runtime: Joi.number().positive().required(),
        Genre: Joi.string().required(),
        Rating: Joi.number().positive().required(),
        Metascore: Joi.number().positive().required(),
        Votes: Joi.number().positive().required(),
        Gross_Earning_in_Mil: Joi.number().positive().required(),
        Director: Joi.number().positive().min(1).max(35).required(),
        Actor: Joi.string().required(),
        Year: Joi.number().positive().required()

    }
     Joi.validate(request.body, schema, (err, result, next) => {
        if (err) {
            winston.error(err);
            console.log(err);
            response.status(404).json({
                status: 'error',
                message: 'Not Found',
                movies: movies
            });
            next();
        }
    })
}
module.exports = {
    validationForId: validationForId,
    validationForPutForMovies: validationForPutForMovies,
    validationForPutForDirectors: validationForPutForDirectors,
    validationForPostForDirector: validationForPostForDirector,
    validationForPostForMovies: validationForPostForMovies
}