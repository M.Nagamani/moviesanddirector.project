const winston = require("winston");
const path = require("path");
const logger = winston.createLogger({
  transports: [
    new winston.transports.File({
      level: 'error',
      filename: path.join(__dirname, "../logs/error.log")

    })
  ]
});
module.exports = logger;